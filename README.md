# GIS-routing

Small routing and image upload web server for GIS

## Setup
Inside the folder, run `npm install`.

Copy the `.env.example` file, rename to `.env` and change to correct info.
