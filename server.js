const { createWriteStream, readdirSync, existsSync } = require('fs');
const { join } = require('path');
const { Client } = require('pg');
const PathFinder = require('geojson-path-finder');
const express = require('express');
const multiparty = require('multiparty');
require('dotenv').config();

async function startServer() {
    const db = new Client();
    await db.connect();
    console.log('Connected to database.');

    const network = await createNetwork(db);

    const app = express();
    app.use(express.json());

    app.post('/route', async (req, res) => {
        try {
            if (!Array.isArray(req.body.points)) {
                const errorMessage = 'In body: "points" isn\'t an array';
                console.error(errorMessage);
                res.status(400).send(errorMessage);
                return;
            }
            const numberOfPoints = req.body.points.length;
            if (numberOfPoints < 2) {
                const errorMessage = 'Less than 2 points in array';
                console.error(errorMessage);
                res.status(400).send(errorMessage);
                return;
            }
            let startPoint = await findNearestPointFeatureOnPaths(
                db,
                req.body.points[0]
            );
            if (startPoint === null) {
                const errorMessage =
                    'Invalid coordinates: ' + req.body.points[0];
                console.error(errorMessage);
                res.status(400).send(errorMessage);
                return;
            }
            let finishPoint;
            const allPaths = [];
            for (let i = 1; i < numberOfPoints; i++) {
                finishPoint = await findNearestPointFeatureOnPaths(
                    db,
                    req.body.points[i]
                );
                if (finishPoint === null) {
                    const errorMessage =
                        'Invalid coordinates: ' + req.body.points[i];
                    console.error(errorMessage);
                    res.status(400).send(errorMessage);
                    return;
                }
                const result = network.findPath(startPoint, finishPoint);
                if (result === null) {
                    const errorMessage = "Can't find route";
                    console.error(errorMessage);
                    res.status(404).send(errorMessage);
                    return;
                } else {
                    allPaths.push(createMultiPointFeature(result.path));
                }
                startPoint = finishPoint;
            }
            res.json(createFeatureCollection(allPaths));
        } catch (err) {
            console.error(err);
            res.status(500).send(err.message ? err.message : 'Unknown error');
        }
    });

    app.post('/image', async (req, res) => {
        let errorMessage = null;
        const form = new multiparty.Form({ maxFields: 1, autoFiles: false });
        form.on('part', (filePart) => {
            if (!filePart.filename) {
                return;
            }
            // save file
            const currentTime = new Date();
            const imageName =
                currentTime
                    .toISOString()
                    .split('.')[0]
                    .replace(/T/, '-')
                    .replace(/\:/g, '-') + '.jpg';
            const writeStream = createWriteStream(
                join(__dirname, 'images', imageName)
            );
            filePart.pipe(writeStream);
            console.log('Saved ' + imageName);
        });
        form.on('error', (err) => {
            errorMessage = err.message;
            console.error(errorMessage);
        });
        form.on('close', () => {
            if (!res.writableEnded) {
                if (errorMessage === null) {
                    res.status(200).send('File saved');
                } else {
                    res.status(500).send(errorMessage);
                }
            }
        });
        try {
            form.parse(req);
        } catch (err) {
            console.error(err.message);
            res.status(500);
            if (!res.writableEnded) {
                res.send(err.message);
            }
        }
    });

    app.get('/', (req, res) => {
        res.sendFile(join(__dirname, 'index.html'));
    });

    app.get('/images', (req, res) => {
        const fileNames = readdirSync(join(__dirname, 'images'));
        res.json(fileNames);
    });

    app.get('/image/:imageName', (req, res) => {
        if (existsSync(join(__dirname, 'images', req.params.imageName))) {
            res.sendFile(join(__dirname, 'images', req.params.imageName));
        } else {
            res.status(404).send('Image not found!');
        }
    });

    const serverPort = process.env.PORT ? process.env.PORT : 8888;
    app.listen(serverPort, '0.0.0.0', () => {
        console.log('Listening on port ' + serverPort);
    });
}

async function createNetwork(db) {
    const dbPaths = (
        await db.query(
            'SELECT "id", "name", ST_AsGeoJSON("geometry") AS "geojson" FROM "path";'
        )
    ).rows;
    const lineStringFeatures = [];
    for (const dbPath of dbPaths) {
        const pathGeoJson = JSON.parse(dbPath.geojson);
        for (const lineCoordinates of pathGeoJson.coordinates) {
            lineStringFeatures.push(createLineStringFeature(lineCoordinates));
        }
    }
    let networkPrecision = Number.parseFloat(process.env.PRECISION);
    if (Number.isNaN(networkPrecision) || !Number.isFinite(networkPrecision)) {
        networkPrecision = 1e-3;
    }
    return new PathFinder(createFeatureCollection(lineStringFeatures), {
        precision: networkPrecision,
    });
}

async function findNearestPointFeatureOnPaths(db, coordinates) {
    const lonNum = Number.parseFloat(coordinates[0]);
    const latNum = Number.parseFloat(coordinates[1]);
    if (
        Number.isNaN(lonNum) ||
        !Number.isFinite(lonNum) ||
        lonNum < -180 ||
        lonNum > 180 ||
        Number.isNaN(latNum) ||
        !Number.isFinite(latNum) ||
        latNum < -90 ||
        latNum > 90
    ) {
        return null;
    }
    const res = await db.query(
        'SELECT ST_AsGeoJSON(ST_ClosestPoint("p1"."geometry", ST_SetSRID(ST_Point($1, $2), 4326))) AS "closestPoint" ' +
            'FROM "path" AS "p1" ' +
            'WHERE "p1"."id" IN ( ' +
            '    SELECT "id" FROM ' +
            '    ( ' +
            '        SELECT "p2"."id", ST_Distance(ST_SetSRID(ST_Point($1, $2), 4326), "p2"."geometry") AS "distance" ' +
            '        FROM "path" AS "p2" ' +
            '        ORDER BY "distance" ' +
            '        LIMIT 1 ' +
            '    ) AS "closestPath" ' +
            ');',
        [lonNum, latNum]
    );
    if (res.rowCount !== 1) {
        return null;
    }
    return {
        type: 'Feature',
        geometry: JSON.parse(res.rows[0].closestPoint),
    };
}

function createMultiPointFeature(coordinates) {
    return {
        type: 'Feature',
        geometry: {
            type: 'MultiPoint',
            coordinates: coordinates,
        },
    };
}

function createLineStringFeature(coordinates) {
    return {
        type: 'Feature',
        geometry: {
            type: 'LineString',
            coordinates: coordinates,
        },
    };
}

function createFeatureCollection(features) {
    return {
        type: 'FeatureCollection',
        features: features,
    };
}

startServer();
